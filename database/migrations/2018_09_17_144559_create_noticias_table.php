<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo',150);
            $table->text('contenido');

            //campos para la relacion con las tablas users y categorias
             $table->integer('user_id')->unsigned();
              $table->integer('categoria_id')->unsigned();

              //relacion mediante las llaves foraneas
               $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
